Locales['br'] = {
  -- Vestiário
  ['cloakroom'] = 'Vestiário',
  ['ems_clothes_civil'] = 'Roupa Normal',
  ['ems_clothes_ems'] = 'Entrar como Socorrista',

  -- Veículos
  ['ambulance'] = 'ambulância',
  ['helicopter_prompt'] = 'pressione ~INPUT_CONTEXT~ para acessar o ~y~Ações de helicóptero~s~.',
  ['helicopter_buy'] = 'loja de helicóptero',
  ['helicopter_garage'] = 'Abrir garagem',
  ['helicopter_store'] = 'loja de helicóptero na garagem',
  ['helicopter_garage_title'] = 'Garagem de helicóptero',
  ['helicopter_title'] = 'Ações de helicóptero',
  ['helicopter_notauthorized'] = 'você não está autorizado a comprar helicópteros.',
  ['garage_prompt'] = 'pressione ~INPUT_CONTEXT~ para acessar a ~y~Ações do veículo~s~.',
  ['garage_title'] = 'Ações do veículo',
  ['garage_stored'] = 'armazenado',
  ['garage_notstored'] = 'não na garagem',
  ['garage_storing'] = 'Estamos tentando remover o veículo, certifique-se de que nenhum jogador esteja por perto.',
  ['garage_has_stored'] = 'o veículo foi armazenado na sua garagem',
  ['garage_has_notstored'] = 'não foram encontrados veículos nas proximidades',
  ['garage_notavailable'] = 'seu veículo não é armazenado na garagem.',
  ['garage_blocked'] = 'Não há pontos de spawn disponíveis!',
  ['garage_empty'] = 'você não tem nenhum veículo em sua garagem.',
  ['garage_released'] = 'seu veículo foi liberado da garagem.',
  ['garage_store_nearby'] = 'não há veículos nas proximidades.',
  ['garage_storeditem'] = 'Abrir garagem',
  ['garage_storeitem'] = 'armazenar veículo na garagem',
  ['garage_buyitem'] = 'loja de veículos',
  ['shop_item'] = '$%s',
  ['vehicleshop_title'] = 'Loja de veículos',
  ['vehicleshop_confirm'] = 'quer comprar este veiculo?',
  ['vehicleshop_bought'] = 'Você comprou ~y~%s~s~ por ~r~$%s~s~',
  ['vehicleshop_money'] = 'você não pode pagar esse veículo',
  ['vehicleshop_awaiting_model'] = 'o veículo está atualmente ~g~DOWNLOADING & LOADING~s~ por favor, espere',
  ['confirm_no'] = 'Não',
  ['confirm_yes'] = 'Sim',

  -- Menu Ação
  ['hospital'] = 'Hôspital',
  ['revive_inprogress'] = 'Reanimação em andamento',
  ['revive_complete'] = 'você ressuscitou ~y~%s~s~',
  ['revive_complete_award'] = 'você ressuscitou ~y~%s~s~, ~g~$%s~s~',
  ['heal_inprogress'] = 'você está se curando!',
  ['heal_complete'] = 'você ser curou ~y~%s~s~',
  ['no_players'] = 'Nenhum jogador nas proximidades',
  ['no_vehicles'] = 'Sem veículos nas proximidades',
  ['player_not_unconscious'] = 'Não está inconsciente',
  ['player_not_conscious'] = 'esse jogador não está consciente!',

  -- Menu chefe
  ['boss_actions'] = 'Menu Administração',

  -- Misc
  ['invalid_amount'] = '~r~Quantidade inválida',
  ['actions_prompt'] = 'pressione ~INPUT_CONTEXT~ access the ~y~Ambulance Actions~s~.',
  ['deposit_amount'] = 'Quantidade inválida para deposito',
  ['money_withdraw'] = 'Quantidade a ser retirada',
  ['fast_travel'] = 'pressione ~INPUT_CONTEXT~ para mover-se rapidamente.',
  ['open_pharmacy'] = 'pressione ~INPUT_CONTEXT~ para abrir a farmácia.',
  ['pharmacy_menu_title'] = 'Farmácia',
  ['pharmacy_take'] = 'tomar <span style="color:blue;">%s</span>',
  ['medikit'] = 'Seringa',
  ['bandage'] = 'Vendagem',
  ['max_item'] = 'você já carrega o suficiente sobre você.',

  -- F6 Menu
  ['ems_menu'] = 'Interação com cidadão',
  ['ems_menu_title'] = 'Ambulância - Interação com cidadão',
  ['ems_menu_revive'] = 'Reanimar',
  ['ems_menu_putincar'] = 'Colocar no veiculo',
  ['ems_menu_small'] = 'Curar pequenas feridas',
  ['ems_menu_big'] = 'Tratando lesões graves',

  -- telefone
  ['alert_ambulance'] = 'alert Ambulance',

  -- Morte
  ['respawn_available_in'] = 'respawn disponível em ~b~%s minutos %s segundos~s~',
  ['respawn_bleedout_in'] = 'você esta sangrandor por ~b~%s minutos %s segundos~s~\n',
  ['respawn_bleedout_prompt'] = 'pressione [~b~E~s~] para reaparecer',
  ['respawn_bleedout_fine'] = 'pressione [~b~E~s~] para reaparecer por ~g~R$%s~s~',
  ['respawn_bleedout_fine_msg'] = 'você pagou ~r~R$%s~s~ para reaparecer.',
  ['distress_send'] = 'pressione [~b~G~s~] para enviar sinal de socorro',
  ['distress_sent'] = 'sinal de socorro foi enviado para as unidades disponíveis!',
  ['distress_message'] = 'atenção médica necessária: cidadão inconsciente!',
  ['combatlog_message'] = 'você foi forçado a renascer porque você já deixou o servidor quando está morto.',

  -- Revive
  ['revive_help'] = 'reviver um jogador',

  -- Item
  ['used_medikit'] = 'você usou 1x Seringa',
  ['used_bandage'] = 'você usou 1x Vendagem',
  ['not_enough_medikit'] = 'Você não tem ~b~Seringa~s~.',
  ['not_enough_bandage'] = 'Você não tem ~b~Vendagem~s~.',
  ['healed'] = 'Você foi tratado.',
}
